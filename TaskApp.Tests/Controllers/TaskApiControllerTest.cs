﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using TaskApp.Models;
using TaskApp.Controllers;

namespace TaskApp.Tests.Controllers
{
    [TestClass]
    public class TaskApiControllerTest
    {
        [TestMethod]
        public void Get()
        {
            ITaskRepository repo = MockRepository.GenerateMock<ITaskRepository>();
            ITask task1 = MockRepository.GenerateMock<ITask>();
            task1.Stub(m => m.Id).Return(1);
            task1.Stub(m => m.IsComplete).Return(true);
            string desc1 = "Description";
            task1.Stub(m => m.Description).Return(desc1);

            ITask task2 = MockRepository.GenerateMock<ITask>();
            task2.Stub(m => m.Id).Return(2);
            task2.Stub(m => m.IsComplete).Return(false);
            string desc2 = "Another description";
            task2.Stub(m => m.Description).Return(desc2);

            repo.Stub(m => m.GetTasks()).Return(new ITask[] { task1, task2 });

            TaskApiController controller = new TaskApiController(repo);
            IEnumerable<ITask> tasks = controller.Get();

            repo.AssertWasCalled(m => m.GetTasks());
            Assert.AreEqual(2, tasks.Count());

            ITask taskRet1 = tasks.First();
            ITask taskRet2 = tasks.Last();

            Assert.AreEqual(1, taskRet1.Id);
            Assert.AreEqual(true, taskRet1.IsComplete);
            Assert.AreEqual(desc1, taskRet1.Description);

            Assert.AreEqual(2, taskRet2.Id);
            Assert.AreEqual(false, taskRet2.IsComplete);
            Assert.AreEqual(desc2, taskRet2.Description);
        }

        [TestMethod]
        public void Put()
        {
            ITaskRepository repo = MockRepository.GenerateMock<ITaskRepository>();
            BaseTask bt = new BaseTask();

            TaskApiController controller = new TaskApiController(repo);
            controller.Put(bt);

            repo.AssertWasCalled(m => m.UpdateTask(bt));
        }

        [TestMethod]
        public void Delete()
        {
            ITaskRepository repo = MockRepository.GenerateMock<ITaskRepository>();
            BaseTask bt = new BaseTask();

            TaskApiController controller = new TaskApiController(repo);
            controller.Delete(bt);

            repo.AssertWasCalled(m => m.RemoveTask(bt));
        }

        [TestMethod]
        public void Post()
        {
            ITaskRepository repo = MockRepository.GenerateMock<ITaskRepository>();
            BaseTask bt = new BaseTask();

            TaskApiController controller = new TaskApiController(repo);
            controller.Post(bt);

            repo.AssertWasCalled(m => m.AddTask(bt));
        }
    }
}
