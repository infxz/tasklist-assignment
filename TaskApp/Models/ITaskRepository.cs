﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp.Models
{
    public interface ITaskRepository
    {
        IEnumerable<ITask> GetTasks();
        void UpdateTask(ITask task);
        ITask AddTask(ITask task);
        void RemoveTask(ITask task);
    }
}
