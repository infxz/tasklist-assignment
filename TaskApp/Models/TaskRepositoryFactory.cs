﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskApp.Models
{
    public static class TaskRepositoryFactory
    {
        private static ITaskRepository _instance = new Mocks.MockTaskRepository();

        public static ITaskRepository Instance { get { return _instance; } }
    }
}