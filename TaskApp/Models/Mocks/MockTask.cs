﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Runtime.Serialization;

namespace TaskApp.Models.Mocks
{
    [KnownType(typeof(MockTask))]
    public class MockTask : ITask
    {
        private static int IdCounter = 0;
        private static Random Random = new Random();

        public MockTask()
        {
            Id = Interlocked.Increment(ref IdCounter);
            Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at tellus auctor, tempor nibh non.";
            IsComplete = Random.Next(0, 2) == 1;
        }

        public int Id { get; private set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
    }
}