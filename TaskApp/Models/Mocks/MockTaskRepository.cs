﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskApp.Models.Mocks
{
    public class MockTaskRepository : ITaskRepository
    {
        public IEnumerable<ITask> GetTasks()
        {
            MockTaskFactory taskFactory = new MockTaskFactory();

            List<ITask> tasks = new List<ITask>();
            tasks.Add(taskFactory.CreateTask());
            tasks.Add(taskFactory.CreateTask());
            tasks.Add(taskFactory.CreateTask());
            tasks.Add(taskFactory.CreateTask());
            tasks.Add(taskFactory.CreateTask());

            return tasks;
        }

        public void UpdateTask(ITask task) { }

        public ITask AddTask(ITask task) 
        {
            MockTask mt = new MockTask();
            mt.Description = task.Description;
            mt.IsComplete = task.IsComplete;

            return mt;
        }

        public void RemoveTask(ITask task) { }
    }
}