﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskApp.Models.Mocks
{
    public class MockTaskFactory
    {
        public ITask CreateTask()
        {
            return new MockTask();
        }
    }
}