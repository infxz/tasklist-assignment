﻿var taskList = {};
taskList.strikeClass = "text-strike";
taskList.baseTaskItemId = "task-item-";
taskList.baseDescriptionId = "lbl-desc-";
taskList.baseCheckedId = "chk-complete-";
taskList.tasks = [];
taskList.apiUrl = "/api";
taskList.rootElem = null;

$.fn.taskList = function (url) {
    taskList.apiUrl = url;
    taskList.rootElem = this;
    $.getJSON(taskList.apiUrl, function (tasks) { taskList.setTasks(tasks); taskList.displayTasksAndAttach(tasks); });
    return this;
};

taskList.showError = function (desc) {
    $(".error").text(desc);
};

taskList.setTasks = function (tasks) {
    $.each(tasks, function (index) {
        var task = tasks[index];

        taskList.tasks[task.Id.toString()] = task;
    });
};

taskList.displayTasksAndAttach = function (tasks) {
    var arr = [];

    $.each(tasks, function (index) {
        arr.push(taskList.createTaskView(tasks[index]));
    });

    taskList.rootElem.append(arr.join(''));

    taskList.bindEvents();
};

taskList.createTaskView = function (task) {
    var arr = [];

    arr.push("<li id='" + taskList.baseTaskItemId + task.Id + "'>");
    arr.push("<div class='task-item'>");
    arr.push("<div class='task-complete'>");
    arr.push("<div class='task-complete-wrap'>");
    arr.push("<input class='chk-complete'");
    if (task.IsComplete)
        arr.push(" checked='true' ");
    arr.push("type='checkbox' id='chk-complete-" + task.Id + "' />");
    arr.push("</div>");
    arr.push("</div>");
    arr.push("<div class='task-desc'>");
    arr.push("<p id='lbl-desc-" + task.Id + "'");
    if (task.IsComplete)
        arr.push(" class='" + taskList.strikeClass + "'");
    arr.push(">" + task.Description + "</p>");
    arr.push("</div>");
    arr.push("<div class='task-remove'>");
    arr.push("<input class='btn-remove' type='button' value='Remove' id='btn-remove-" + task.Id + "' />");
    arr.push("</div>");
    arr.push("</div>");
    arr.push("</li>");

    return arr.join('');
};

taskList.bindEvents = function () {
    $(".btn-remove").click(function () {
        var taskId = taskList.getTaskIdFromElement(this);
        taskList.removeTask(taskId);
    });

    $(".chk-complete").change(function () {
        var taskId = taskList.getTaskIdFromElement(this);

        if ($("#" + taskList.baseCheckedId + taskId).is(':checked')) {
            taskList.flagTaskAsComplete(taskId, true);
        } else {
            taskList.flagTaskAsComplete(taskId, false);
        }
    });
};

taskList.getTaskIdFromElement = function (element) {
    return element.id.substring(element.id.lastIndexOf('-') + 1);
};

taskList.removeTask = function (id) {

    $.ajax({
        type: "DELETE",
        url: taskList.apiUrl,
        contentType: "application/json",
        data: JSON.stringify(taskList.tasks[id]),
        success: function (data) {
            delete taskList.tasks[id];
            $("#" + taskList.baseTaskItemId + id).hide(400);
        },
        error: function () {
            taskList.showError("Unable to delete task.");
        }
    });

};

taskList.addTask = function (desc) {
    var task = {};
    task.Description = desc;
    task.Id = -1;
    task.IsComplete = false;

    $.ajax({
        type: "POST",
        url: taskList.apiUrl,
        contentType: "application/json",
        data: JSON.stringify(task),
        error: function () {
            taskList.showError("Unable to update task.");
        },
        success: function (data) {
            var taskItem = taskList.createTaskView(data);
            taskList.rootElem.prepend(taskItem);
            taskList.bindEvents();
        }
    })
}

taskList.flagTaskAsComplete = function (id, complete) {
    var descId = "#" + taskList.baseDescriptionId + id;

    if (complete)
        $(descId).addClass("text-strike");
    else
        $(descId).removeClass("text-strike");

    taskList.tasks[id].IsComplete = complete;

    var json = JSON.stringify(taskList.tasks[id]);

    $.ajax({
        type: "PUT",
        url: taskList.apiUrl,
        contentType: "application/json",
        data: JSON.stringify(taskList.tasks[id]),
        error: function () {
            taskList.showError("Unable to update task.");
        }
    });
};

$(function () {

    $(".btn-remove").click(function () {
        alert("test");
    })

    $("#task-form").submit(function () {
        taskList.addTask($("#txt-description").val());

        return false;
    });


});