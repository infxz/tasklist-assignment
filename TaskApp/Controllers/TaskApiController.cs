﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskApp.Models;
using TaskApp.Models.Mocks;

namespace TaskApp.Controllers
{
    public class TaskApiController : ApiController
    {
        private ITaskRepository _repository;

        public TaskApiController() : this(TaskRepositoryFactory.Instance) { }

        public TaskApiController(ITaskRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<ITask> Get()
        {
            return _repository.GetTasks();
        }

        public ITask Post(BaseTask task)
        {
            return _repository.AddTask(task);
        }

        public void Put(BaseTask task)
        {
            _repository.UpdateTask(task);
        }

        public void Delete(BaseTask task)
        {
            _repository.RemoveTask(task);
        }
    }
}
